CREATE DEFINER=`root`@`localhost` PROCEDURE `getNumberOfOrdersByShipper`(IN shipper VARCHAR(45), OUT count INT)
BEGIN
	select count(orderID) into count
	from orders join shippers on orders.ShipperID = shippers.ShipperID
	where ShipperName = shipper; 
END